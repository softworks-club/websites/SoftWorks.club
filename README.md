# SoftWorks.club website

## Website:
**[SoftWorks.club](https://softworks.club "SoftWorks.club")** is our hobbyist club site and blog.

### Short description:
Free(dom) and open source hobbyist club website.

### Long description:
This is the website of a club of hobbyists that love to hack with free(dom) and open source projects. Its purpose is to host a blog, wiki, tutorials about free software, code, shell scripts for Linux. And of course a club blog.

### Thanks to:
* [JAMstack](https://jamstack.org "JavaScript, API and Markup stack") for their modern web development architecture.
* [Hugo](https://gohugo.io "Hugo") for the static website generator.
* [MinifTanjum](https://minimo.netlify.com/ "Munif Tanjim") for Minimo Hugo theme.
* [GitLab](https://gitlab.com "GitLab.com") for their git services.
* [Netlify](https://netlify.com "Deploy with the power of Netlify") for their powerfull professional deployment features.
* [TuxFamily](https://tuxfamily.org "TuxFamily.org") for their free(dom) web space and services.

### Website available at:
* [Master](https://softworks.club "SoftWorks.club") for the main version of the website.
* [Developing](https://workinprogress.softworks.club "SoftWorks.club - Beta") for the next version of the website.

### ToDo:
The team shares a [Undefined](https://softworks.club "Undefined") board.

### Collaborators:
Contact us through [GitLab](https://gitlab.com/SoftWorks-Club "SoftWorks.club Group") on GitLab.


## © SoftWorks 2015-2023 by speedytux@posteo.net:

The contents of SoftWorks.club, which consists of the files in
 * archetypes/
 * content/
 * data/
 * resources/
 * static/
 * themes/
 * .gitmodules
 * LICENSE
 * config.toml
 * README.md (this file)

are [licensed under a MIT License](LICENSE "MIT"), original available [here](https://opensource.org/licenses/MIT/ "MIT"), if it is not explicitly mentioned.

All the software we promote or host is licensed with [Free Software Foundation](https://fsf.org "Free Software Foundation site") [approved licenses](http://www.gnu.org/licenses/license-list.en.html "Free Software Licenses").
