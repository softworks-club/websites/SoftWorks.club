+++
title = "Mistakes Moving to Linux"
date = 2023-02-20T17:22:00+01:00
draft = true
language = "en"
authors = [ "speedytux" ]
tags = [ "SoftWorks", "reflections", "distributions" ]
categories = [ "blog" ]
+++

__Mistakes Moving to Linux__

> Minimalism is not a lack of something. It’s simply the perfect amount of something.

- Picking the Wrong Hardware

- Not Knowing What Linux Is

- Picking the Wrong Distribution

- Jumping Without Conclusions

- Realizing It is Not Windows

- Afraid to Use the Terminal

- Not Able to Find the Right Apps

- Take Care of File Compatibility

- Not Asking the Community for Help

- Giving Up Quickly

Source: itsfoss.com/rookie-linux-mistakes
