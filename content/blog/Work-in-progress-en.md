+++
title = "Work in progress on FreeSource.eu"
description = "A group of hobbyists prepares their Free(dom) and open source website."
date = 2017-05-08
tags = [ "staticgen", "freesource", "freeworx", "jekyll", "wordpress", "github", "github-pages" ]
draft = false
language = "en"
authors = [ "speedytux" ]
+++

>> DEPRECATED

_Welcome to FreeSource!_

This is the website of a group of hobbyists that love to hack with **free(dom) and open source projects**.

The site is _now in maintanance_. Please keep live your interest and _be patient!_

Really it is a transfer from **freeworx.net**, that no longer exists, and **freesource.eu**, from **[Wordpress](http://wordpress.com "Wordpress")** and its complex and conflicting extensions updates to **[Pages on GitHub](http://pages.github.com "Pages on GitHub.com")** and his **[Jekyll](https://jekyllrb.com/ "Jekyll")**, their simple, blog-aware, static site generator.

Maybe it will need more effort, but, at the same time, more control.

This site will include guides, opinions, scripts or even fragment of code for the free(dom) software community.

All the readers interested in this project con contribute writing at <info@freesource.eu>.

speedytux
