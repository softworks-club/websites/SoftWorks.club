+++
title = "My First Hugo Article"
date = 2018-07-23T19:17:57+03:00
draft = false
language = "en"
authors = [ "speedytux" ]
tags = [ "hugo", "staticgen", "github", "experiments" ]
categories = [ "blog" ]
+++

__Hello Hugo world!__

> Minimalism is not a lack of something. It’s simply the perfect amount of something.
> — Nicholas Burroughs

This is My Start With Hugo Static Site Generator.

Indeed, there are no more excuses for having no blog or documentation now!

We choose to base our effort on Hugo and Netlify. A great service in the JAMstack environment.

> Our purpose is continuing to test over the edge freedom technologies. So this hobbyist site is intended for hosting our efforts: a blog, a wiki, some tutorials about free software... and documentations for code, shell scripts for Linux, normally hosted on Github or Gitlab.

We continue to test static web site generators like Hugo, Jekyll or Gatsby for training purpose. But for now, Hugo proved to be quick, ready to deploy, with little learning curve.

We love Free(dom) software, so we will publish with GPL, CC-BY or other Free Software Foundation approved licenses.

Stay tunes or join us!

speedytux
