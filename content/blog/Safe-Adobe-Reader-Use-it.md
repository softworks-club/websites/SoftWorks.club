+++
title = "Uso sicuro di Adobe Reader su Linux"
date = 2016-02-22
tags = [ "adobe-reader", "experiments", "evince", "firejail", "okular", "pdf", "sandbox" ]
draft = false
language = "it"
authors = [ "speedytux" ]
+++

Illustrerò come impedire ad Adobe Reader 9, disponibile per molte distribuzioni GNU/Linux, di usare la rete e di avere accesso libero al filesystem con filejail.

### Perché usare Adobe Reader in GNU/Linux
Utilizzo Adobe Reader per lavoro. E' una necessità perché né Evince (GNOME) né Okular (KDE) gestiscono decentemente il riempimento di moduli PDF e l'uso di caratteri speciali, come i caratteri diacritici rumeni. Sembra un bug delle librerie poppler a cui dopo molti anni nessun programmatore a mai messo mano.

### Cos'è firejail
Firejail è un programma di sandbox facile da usare che riduce il rischio di violazioni della sicurezza, limitando l'ambiente di esecuzione di applicazioni non attendibili. Può essere usato con browser, applicazioni desktop, ma anche con demoni e server.

### Creare un profilo
Ho creato il seguente profilo copiando quello standard di evince da /etc/firejail e l'ho posto in .config/firejail/acroread.profile . Il file di profilo per evice annesso al progetto include una lista di impostazioni per la sicurezza che impediscono l'accesso a ~/.adobe e sono include nel file disable-common.inc . Ho copiato il file nella cartella dei profili locale .config/firejail/ e ho commentato la riga di adobe. Poi ho aggiunto la possibilità di accedere al file system della mia Home per leggere e salvare i file. E con le ultime tre righe ho bloccato l'accesso a funzioni del kernel, alla rete e alla possibilità di usare i privilegi di root, non si sa mai.

    # acroread profile
    # based upon standard evince.profile
    include /etc/firejail/disable-mgmt.inc
    include /etc/firejail/disable-secret.inc
    include ${HOME}/.config/firejail/disable-adobe-common.inc    #Ho personalizzato disable-common.inc
    include /etc/firejail/disable-devel.inc
    blacklist ${HOME}/.pki/nssdb
    blacklist ${HOME}/.lastpass
    blacklist ${HOME}/.keepassx
    blacklist ${HOME}/.password-store
    blacklist ${HOME}/.wine
    whitelist /opt/Adobe    #Ho aggiunto l'accesso ad alcune directory necessarie
    whitelist ${HOME}/Desktop
    whitelist ${HOME}/PDF
    whitelist ${HOME}/Documents
    caps.drop all    #Ho bloccato tutte le funzioni inutili come la rete
    net none
    noroot

Con il comando

    firejail acroread

Otteniamo Adobe Reader in una sandbox personalizzata.

### Modificare il launcher
Per fare in modo che ogni volta si lanci Adobe Reader venga lanciata la sandbox ho modificato il launcher. E' sufficiente creare un file eseguibile in /usr/local/bin/acroread con la sola seguente riga:

    firejail acroread $@

### Testato su...
openSUSE Tumbleweed nel febbraio 2016

### Riferimenti
[Archlinux.org Wiki](https://wiki.archlinux.org/index.php/Firejail "Firejail")
