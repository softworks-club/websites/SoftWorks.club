---
title:      Showcase
date:       2020-05-20 12:00:00
author:     speedytux
categories: page
draft:      false
language:   "en"
menu:	    main
weight:     4
tags:
 - showcase
---

# SoftWorks Showcase

On this page you can find links to our other websites.

* ## [FreeSource](https://freesource.softworks.club "FreeSource.eu")
This is our previous website, a beautiful dark theme obtained from Jacob Tomlinson that have the name Carte Noire, prepared with Jekyll.

* ## [Scatti & Panorami](https://www.scattiepanorami.it "Scatti & Panorami")
A blog about photography, by a photo lover, a trainer and teacher of photographers. (Italian language)
