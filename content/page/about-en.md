---
title:      About
date:       2020-05-20 12:00:00
author:     speedytux
categories: page
draft:      false
language:   "en"
menu:	    main
weight:     3
tags:
 - team
 - SoftWorks
---

# SoftWorks Club
[At this page on GitLab](https://gitlab.com/softworks-club/websites/SoftWorks.club/-/blob/workinprogress/README.md "README.md") you can find the description of our project.

## SoftWorks Team

Our team members are:

* ### [speedytux](https:// "speedytux")
Founder, mantainer, programmer, Linux distro-hopper.

* ### [gnuman](https:// "gnuman")
Founder, mantainer, teacher, Archlinux GNOME user.

* ### [Luca](https:// "Luca")
Photographer, blogger, trainer.
