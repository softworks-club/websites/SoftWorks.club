---
title:      speedytux biografy
date:       2015-04-06 12:00:00
author:     speedytux
categories: page
draft:      false
language:   "it"
tags:
 - speedytux
 - distributions
---

A scuola ho imparato l'informatica e la programmazione con un sistema **HP-UX** (Unix), ma al lavoro ho dovuto usare solo **Windows**. Ho praticamente disimparato cose importanti.

Fino a che un ex compagno di scuola mi è diventato collega al lavoro ed era un grande fan di **Red Hat**. Era la metà degli anni '90. Così mi ha convinto a provare Red Hat. Mi è stato molto difficile allora, _senza internet_. Per di più non mi è piaciuto **GNOME**.

Quando ho avuto una buona connessione internet al lavoro ho provato di nuovo Linux, stavolta era **Mandrake KDE** (che poi è diventato **Mandriva**, all'origine pure di **Mageia**). Mi sono innamorato a prima vista, ma senza internet a casa, non ho potuto sostituire Windows. Quando sono riuscito ad avere una buona connessione pure a casa, ho scoperto il software libero o Free Software: **Netscape**, **Firefox**, **OpenOffice**, **DesktopBSD** ... fino ad arrivare ad **Ubuntu**.

Di Ubuntu mi ha conquistato il marketing di **Canonical** e **Shuttleworth** insieme all'interfaccia "africana". Ma il vero passaggio a Linux lo devo ad un amico rumeno che mi ha prestato un CD con **SimplyMepis**, una seria distribuzione KDE basata su Ubuntu e **Debian**. Semplicemente fantastica. Ho cancellato immediatamente la partizione di Windows, e l'ho trasferita in **Virtualbox**, dove è relegata fino ad oggi.

Ero abituato a mantenere Windows sempre aggiornato, come pure tutte le applicazioni. Sempre aggiornate all'ultima versione. Ho cercato di mantenere questa eccellente abitudine pure con Linux, tendendo ad usare distribuzioni che mi offrivano l'ultimo Firefox, l'ultimo **LibreOffice**... Credo di averle provate tutte: **openSUSE Tumbleweed**, **Sidux** (ovvero debian sid), **Sabayon** (pacchetti di **gentoo**), **Fedora**, Mandriva, **Chakra**... fino a che ho deciso di provare **Archlinux**.

Meravigliosa! Posso dire che mi ha convinto prima di tutto **Pacman**, il _package manager_. Poi la _wiki_, _il repository degli utenti_ **AUR** e _la comunità_ che io definirei RAZIONALE, non emotiva. Arch KDE è stato il mio sistema principale dall'estate del 2012 a quella del 2015. Benché lo abbia usato con soddisfazione, quando KDE è passato a Plasma 5 ho cercato di familiarizzarmi con GNOME, Fedora/Red Hat, openSUSE e Calculate Linux per lavoro.

Sono comunque tornato a Archlinux, benché stavolta con GNOME, uno stabile ambiente di lavoro 

Ma ai principianti che vengono da Windows raccomando **Korora** di Chris Smart, un australiano molto intelligente con il quale ho chattato quando traducevo in italiano il manuale per gli utenti di Sidux. Oppure **openSUSE Leap** e **Ubuntu GNOME**.

Mi trovate solo su [Twitter](http://twitter.com/speedytux "@speedytux")
