---
title:      Project Monitor
date:       2023-02-20 12:00:00
author:     speedytux
categories: page
draft:      false
language:   "en"
#menu:	    main
#weight:     4
tags:
 - SoftWorks staticgen
---

__SoftWorks Project Monitor__

This is the current situation of our projects.

[![Netlify Status](https://api.netlify.com/api/v1/badges/1cc9878c-a7ad-4d10-96d6-0035bbbec1cf/deploy-status)](https://app.netlify.com/sites/softworksclub/deploys?branch=master) __Master Branch__

[![Netlify Status](https://api.netlify.com/api/v1/badges/1cc9878c-a7ad-4d10-96d6-0035bbbec1cf/deploy-status)](https://app.netlify.com/sites/softworksclub/deploys?branch=workinprogress) __Test (WorkInProgress) Branch__

