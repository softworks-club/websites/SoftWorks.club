+++
title = "openSUSE: una buona alternativa a Windows"
date = 2013-01-03
draft = true
authors = [ "lxlive" ]
tags = [ "hugo", "staticgen", "github", "experiments" ]
categories = [ "blog" ]
+++

# openSUSE una valida alternativa a Windows

Sono ormai anni che cerco delle valide alternative a Windows, affidandomi a Linux.

Certo ci sono moltissime distribuzioni più che valide. Ma molte di queste distribuzioni vengono sviluppate in un'ottica di "esperti" o per meglio dire "smanettoni". E un utente medio si trova ad avere grosse difficoltà a cambiare S.O.(Sistema Operativo) e abituarsi in breve tempo. Certo nessun utente "medio" si metterebbe Linux senza sapere quello che sta facendo; ma un S.O. che è sia gratuito, che supportato e sopratutto flessibile e sicuro, alletta sicuramente le persone che sempre più sono entrate in questo fantastico mondo virtuale.

> Una delle distribuzioni Linux degna di nota è openSUSE.

OpenSUSE inizia il suo sviluppo nel lontano 1992 come S.U.S.E. è questa la rende una delle distribuzioni più longeve a livello mondiale ed europeo, visto che è una distribuzione di origine tedesca. Attualmente lo sviluppo è arrivato alla versione 12.2.

Ma cosa rende questa distribuzione un valida alternativa a windows? Gli sviluppatori iniziali (Roland Dyroff, Thomas Fehr, Burchard Steinbild e Hubert Mantel) hanno avuto come prerogativa un ambiente desktop <i>user-friendly</i>, cioè con usabilità, efficacia e semplificazione dell'apprendimento, molto simile ai sistemi Windows, visto che è il S.O. più diffuso e usato al mondo.

Questo già basterebbe per rispondere alla domanda, ma noi di FreeWorX.net non ci fermiamo alle apparenze. Scendiamo più nello specifico. Una funzione, un programma, che la rende per certi versi simile a Windows è YaST. Un vero e proprio centro di comando della distribuzione, che, per funzioni e concettualità è simile al "Pannello di Controllo" di windows. Dove è possibile intervenire su configurazioni hardware, amministrazione di sistema, installazione di software, configurazione di rete, di server e altro. Proprio come il famoso "Pannello di Controllo". Una curiosità. YaST non è un programma svillupato da openSUSE ma da Novell azienda statunitense che opera nello sviluppo di software.

Dal 2011 il gruppo Attachmate, ha acquistato SUSE. Questo gruppo ha tra i suoi maggiori investitori proprio Microsoft. Da allora cura lei lo sviluppo di questa distribuzione, insieme alla comunità.

Un'altra caratteristica è l'installazione di programmi molto semplificata, come in Windows, attraverso un'estensione. In Windows, per installare un programma, basta cliccare sul file eseguibile, o _exe_, e l'installazione parte. In openSUSE, visto la sua somiglianza, ha una sua installazione che imita l'_exe_, cioè _ymp_. Che dopo aver cliccato il programma partirà con l'installazione.

Come tutte le versioni di Linux, ha una gestione di pacchetti che permette l'installazione e l'aggiornamento dei programma e del sistema. Una particolarità di openSUSE, che per alcuni puristi della logica "Open Source" o della "GNU" viene criticata, è la sua compatibilità con hardware attraverso driver proprietari in accordo con le case produttrici. Nello specifico le schede video ATI e NVIDIA, poi programmi come Flashplayer, Java, Real Player, VLC e altri.&nbsp;Per onore della discussione meglio spiegare qual'è questa particolarità. Per "driver proprietari" si intende che sono le case stesse produttrici, che sviluppano i driver base. Ma visto che ci sono interessi economici dietro, solo le case produttrici hanno i diritti su questi driver. E questo va in forte contrasto con la logica del "sistema libero" di cui Linux, si può dire, e il suo fondatore. Ma per avere un sistema che funzioni bene a volte bisogna fare qualche compromesso. Non che i sistemi che adottano solo open source non funzionino, ma per avere un sistema subito pronto bisogna seguire questa strada intermediaria.

Queste sono le caratteristiche che rendono openSUSE è una valida alternativa a windows.

Luca Labagnara, lxlive
